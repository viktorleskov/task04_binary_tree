package customcollection;

import customcollection.tree.TreeOperations;
import customcollection.tree.component.Tree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestApp {
    private static Logger LOG = LogManager.getLogger(TestApp.class);
    public static void main(String[] args) {
        TestTreeAutoRandom(5);
        System.out.println("");
        TestTreeManualAddAndRemove(Arrays.asList(6,4,5,3,7,12),Arrays.asList(6,7,3,4));
    }
    public static void TestTreeAutoRandom(int size){
        Tree<Object> mytree = TreeOperations.getRandomTree(size,5);
        LOG.info("Start AUTO TEST");
        System.out.println(mytree.asList());
        System.out.println(mytree.stream().collect(Collectors.groupingBy(Object::toString)));
        System.out.println(mytree.stream().collect(Collectors.groupingBy(Object::getClass)));
        LOG.info("Finish AUTO TEST");

    }
    public static void TestTreeManualAddAndRemove(List<Object> list, List<Object> removelist){
        Tree<Object> mytree = new Tree<>();
        mytree.add(list);
        LOG.info("Start MANUAL TEST");
        System.out.println("before list: "+mytree.asList());
        System.out.println("remove list: "+removelist);
        mytree.remove(removelist);
        System.out.println("after  list: "+mytree.asList());
        LOG.info("Finish MANUAL TEST");
    }
}
