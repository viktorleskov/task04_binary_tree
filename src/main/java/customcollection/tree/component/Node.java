package customcollection.tree.component;

public class Node<T> implements Comparable{
    private Node<T> leftson;
    private Node<T> rightson;
    private T value;

    public Node(T value){
        this.value=value;
    }
    public T getValue(){
        return value;
    }
    public Node<T> getLeftson(){
        return leftson;
    }
    public Node<T> getRightson(){
        return rightson;
    }
    public void setLeftson(Node<T> leftson){
        this.leftson=leftson;
    }
    public void setRightson(Node<T> rightson){
        this.rightson=rightson;
    }
    @Override
    public boolean equals(Object o){
        if(o.getClass()!=this.getClass()){
            return false;
        }
        return this.hashCode()==o.hashCode();
    }
    @Override
    public int hashCode(){
        return value.hashCode();
    }
    @Override
    public int compareTo(Object o) {
        return hashCode()-o.hashCode();
    }
}
