package customcollection.tree.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Tree<T> {
    private static Logger LOG = LogManager.getLogger(Tree.class);
    private Node<T> start;

    public Tree(){
    }
    private Node<T> findPlaceForNodeAndInsert(T value, Node<T> start){
        if(isNull(start))return new Node<>(value);
        if(start.compareTo(value)<0){
            if(isNull(start.getLeftson())){
                start.setLeftson(findPlaceForNodeAndInsert(value,start.getLeftson()));
            }
            findPlaceForNodeAndInsert(value,start.getLeftson());
        }
        else if(start.compareTo(value)>0){
            if(isNull(start.getRightson())){
                start.setRightson(findPlaceForNodeAndInsert(value,start.getRightson()));
            }
            findPlaceForNodeAndInsert(value,start.getRightson());
        }
        return null;
    }
    private boolean findNodeAndDelete(T value, Node<T> start){
        if(start.compareTo(value)==0)return true;
        if(start.compareTo(value)<0){
            if(!isNull(start.getLeftson())){
                if(findNodeAndDelete(value,start.getLeftson())){
                    deleteNode(start, Side.LEFT);
                    return false;
                }
            }
        }
        if(start.compareTo(value)>0){
            if(!isNull(start.getRightson())){
                if(findNodeAndDelete(value, start.getRightson())){
                    deleteNode(start,Side.RIGHT);
                    return false;
                }
            }
        }
        return false;
    }
    private boolean isNull(Object o){
        return o==null;
    }
    private void initializeTree(T initialvalue){
        start = new Node<>(initialvalue);
    }
    private void deleteNode(Node<T> node, Side side){
        List<T> buflistforleftson = new LinkedList<T>();
        List<T> buflistforrightson = new LinkedList<T>();
        switch (side){
            case LEFT:{
                asListHelper(node.getLeftson().getLeftson(), buflistforleftson);
                asListHelper(node.getLeftson().getRightson(), buflistforrightson);
                node.setLeftson(null);
                break;
            }
            case RIGHT:{
                asListHelper(node.getRightson().getLeftson(), buflistforleftson);
                asListHelper(node.getRightson().getRightson(), buflistforrightson);
                node.setRightson(null);
                break;
            }
            case THIS:{
                if(!isNull(node.getLeftson()))asListHelper(node.getLeftson(), buflistforleftson);
                if(!isNull(node.getRightson()))asListHelper(node.getRightson(), buflistforrightson);
                initializeTree(buflistforleftson.get(0));
            }
        }
        add(buflistforleftson);
        add(buflistforrightson);
    }
    private void asListHelper(Node<T> start, List<T> list){
        if(isNull(start))return;
        asListHelper(start.getLeftson(), list);
        list.add(start.getValue());
        asListHelper(start.getRightson(), list);
    }
    private void printerTree(Node<T> start){
        if(isNull(start))return;
        printerTree(start.getLeftson());
        System.out.print(start.getValue()+ " ");
        printerTree(start.getRightson());
    }
    public void add(T value) {
        if(isNull(start)){
            initializeTree(value);
        }
        findPlaceForNodeAndInsert(value, start);
    }
    public void add(List<T> list){
        if(isNull(start)){
            initializeTree(list.get(0));
        }
        for(T val:list){
            findPlaceForNodeAndInsert(val, start);
        }
    }
    public void remove(T value){
        if(start.compareTo(value)==0){
            deleteNode(start,Side.THIS);
            return;
        }
        findNodeAndDelete(value, start);
    }
    public void remove(List<T> list){
        for(T val:list){
            remove(val);
        }
    }
    public void print(){
        printerTree(start);
    }
    public List<T> asList(){
        List<T> buflist = new LinkedList<>();
        asListHelper(start, buflist);
        return buflist;
    }
    public Stream<T> stream(){
        return this.asList().stream();
    }
}
