package customcollection.tree;

import customcollection.tree.component.Tree;
import java.util.Random;

public class TreeOperations {
    private TreeOperations(){}
    public static Tree<Object> getRandomTree(int size, int maxlength){
        int DEFAULT_RAND_RANGE = 15;
        Random rand = new Random();
        Tree<Object> buftree = new Tree<>();
        for(int k=0;k<size;k++){
            switch (rand.nextInt(3)){
                case 0:{
                    buftree.add(rand.nextInt(DEFAULT_RAND_RANGE));
                    break;
                }
                case 1:{
                    buftree.add((float)rand.nextInt(DEFAULT_RAND_RANGE));
                    break;
                }
                case 2:{
                    buftree.add(getRandomString(maxlength));
                }
            }
        }
        return buftree;
    }
    public static String getRandomString(int maxlength){
        StringBuilder buf= new StringBuilder();
        Random rand = new Random();
        for(int k=1+rand.nextInt(maxlength);k>0; k--){
            buf.append((char) (65 + rand.nextInt(5)));
        }
        return buf.toString();
    }
}
